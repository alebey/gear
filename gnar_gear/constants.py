LOG_FORMAT = '%(asctime)s %(levelname)-8s %(name)s:%(lineno)d   %(message)s'
LOG_FORMAT_MSEC = '%s.%03d'
PASSWORD_CANNOT_BE_EMPTY = 'Password cannot be empty'
TOKEN_HAS_EXPIRED = 'Token has expired'
